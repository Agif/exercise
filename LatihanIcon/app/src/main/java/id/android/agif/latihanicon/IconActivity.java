package id.android.agif.latihanicon;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


public class IconActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_icon);

        Button buttonClickMe = (Button) findViewById(R.id.buttonClickMe);
        buttonClickMe.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonClickMe:
            //sebuah perintah
                Toast.makeText(getBaseContext(), "You Can ... ", Toast.LENGTH_SHORT).show();
                Intent bukaMainActivity = new Intent(this, MainActivity.class);
                startActivity(bukaMainActivity);
                break;
        }
    }
}
